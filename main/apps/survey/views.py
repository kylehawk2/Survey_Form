# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse, redirect

def index(request):
    if 'count' not in request.session:
        request.session['count'] = 0
    request.session['count'] += 1
    return render(request, 'survey/index.html')

def create(request):
    if request.method == 'POST':
        request.session['name'] = request.POST['name']
        request.session['dojo'] = request.POST['dojo']
        request.session['lang'] = request.POST['lang']
        request.session['comment'] = request.POST['comment']
        return redirect('/result')
    else:
        return redirect('/')

def result(request):
    return render(request, 'survey/result.html')